# matrix

A basic c++ header only matrix implementation using templates.

The core idea was to have the ability to make computations with matrices and checking their compatibility at compile time.

## Including

Include this class by writing `#include "include/matrix.h"` and you're done.

## Usage

Instantiate a matrix as you would any other class. The `Matrix` class takes 2 to 3 template parameters:

`Matrix<7, 9> m;` instantiates a matrix of width 7 and height 9 with the default double type as a scalar type.  
`Matrix<7, 9, float> m;` instantiates a matrix of width 7 and height 9 with the float type as a scalar type.

Furthermore the constructor takes in a boolean value indicating whether the matrix should be filled with zeros on instantiation. The default is `true`

## Basic arithmatic and matrix multiplication

### The following operations are implemented for

`m1 + m2` addition of two matrices with the same dimensions.   
`m1 - m2` subtraction of `m2` from `m1` with the same dimensions.   
`m * s` multiplication of m with a scalar type `s`.   
`m / s` division of m with a scalar type `s`.   

### All of those operations can also be used on an accumulator

`m1 += m2` add `m2` onto `m1`.   
`m1 -= m2` subtract `m2` from `m1`.   
`m *= s` multiply a scalar type `s` onto the matrix m.   
`m /= s` divide m by a scalar `s`. 

### Matrix multiplication

For matrices of the correct dimensions the multiplication operator is defined as would be expected mathematically:

`m1 * m2` Perform matrix multiplication between `m1` and `m2`.   

### Computing the determinant

The matrix' determinant may be computed using the `det()` method.
This process is yet to be improved. For matrices of size 3 and below Sarrus' rule is employed. For larger matrices the class uses Laplace expansion.
The Laplace expansion is slightly improved for matrices with many zeros by identifying the column or row with the most zero fields.

### Computing submatrices

A submatrix can be retrieved by calling the `subMatrix(col, row)` method.
The parameters describe the column and row to be discarded.

### Accessing a field in a matrix

To write to or read from a matrix field you may write `m[col][row]` to access your matrix' data. You can directly assign values through this process.