#pragma once

/**
 *           ____                ___
 *         ,'  , `.            ,--.'|_             ,--,
 *      ,-+-,.' _ |            |  | :,'   __  ,-.,--.'|
 *   ,-+-. ;   , ||            :  : ' : ,' ,'/ /||  |,     ,--,  ,--,
 *  ,--.'|'   |  || ,--.--.  .;__,'  /  '  | |' |`--'_     |'. \/ .`|
 * |   |  ,', |  |,/       \ |  |   |   |  |   ,',' ,'|    '  \/  / ;
 * |   | /  | |--'.--.  .-. |:__,'| :   '  :  /  '  | |     \  \.' /
 * |   : |  | ,    \__\/: . .  '  : |__ |  | '   |  | :      \  ;  ;
 * |   : |  |/     ," .--.; |  |  | '.'|;  : |   '  : |__   / \  \  \
 * |   | |`-'     /  /  ,.  |  ;  :    ;|  , ;   |  | '.'|./__;   ;  \
 * |   ;/        ;  :   .'   \ |  ,   /  ---'    ;  :    ;|   :/\  \ ;
 * '---'         |  ,     .-./  ---`-'           |  ,   / `---'  `--`
 *                `--`---'                        ---`-'
 *
 * @author Justin Guth at gitlab.com/justin_guth
 *
 * Repository at: https://gitlab.com/justin_guth/matrix.git
 *
 */

#include <stdint.h>
#include <string>

#ifndef JG_MATH_MATRIX_EPSILON
#define JG_MATH_MATRIX_EPSILON 0.0000001
#endif

namespace JG
{
	namespace Math
	{
		template <size_t T_Height, typename T_ScalarType = double> struct Vector
		{
			template <size_t T_MatWidth, size_t T_MatHeight, typename T_MatScalarType> friend class Matrix;

		private:
			T_ScalarType* m_BeginData;
			size_t m_ColumnIndex;

			Vector(size_t columnIndex, T_ScalarType* data);
			Vector(const Vector<T_Height, T_ScalarType>& other);
			Vector operator=(Vector<T_Height, T_ScalarType>& other);

		public:
			T_ScalarType& operator[](const size_t rowNumber);
		};

		template <size_t T_Height, size_t T_Width, typename T_ScalarType = double> class Matrix
		{
			template <size_t T_MatHeight, size_t T_MatWidth, typename T_MatScalarType> friend class Matrix;

		public:
			Matrix(bool doInit = true);
			Matrix(const Matrix<T_Height, T_Width, T_ScalarType>& other);
			~Matrix();

			Matrix<T_Height, T_Width, T_ScalarType>& operator=(const Matrix<T_Height, T_Width, T_ScalarType>& other);
			Matrix<T_Height, T_Width, T_ScalarType> operator+(const Matrix<T_Height, T_Width, T_ScalarType>& other) const;
			Matrix<T_Height, T_Width, T_ScalarType>& operator+=(const Matrix<T_Height, T_Width, T_ScalarType>& other) const;
			Matrix<T_Height, T_Width, T_ScalarType> operator-(const Matrix<T_Height, T_Width, T_ScalarType>& other) const;
			Matrix<T_Height, T_Width, T_ScalarType>& operator-=(const Matrix<T_Height, T_Width, T_ScalarType>& other) const;

			Vector<T_Height, T_ScalarType> operator[](const size_t columnNumber);

			template <size_t T_OtherWidth>
			Matrix<T_Height, T_OtherWidth, T_ScalarType> operator*(
				const Matrix<T_Width, T_OtherWidth, T_ScalarType>& other) const;

			template <typename T_OtherScalarType>
			Matrix<T_Height, T_Width, T_ScalarType> operator*(const T_OtherScalarType other) const;
			template <typename T_OtherScalarType>
			Matrix<T_Height, T_Width, T_ScalarType>& operator*=(const T_OtherScalarType other) const;

			template <typename T_OtherScalarType>
			Matrix<T_Height, T_Width, T_ScalarType> operator/(const T_OtherScalarType other) const;
			template <typename T_OtherScalarType>
			Matrix<T_Height, T_Width, T_ScalarType>& operator/=(const T_OtherScalarType other) const;

			Matrix<T_Width, T_Height> T() const;
			T_ScalarType Deteterminant() const;

			Matrix<T_Height - 1, T_Width - 1, T_ScalarType> SubMatrix(size_t column, size_t row) const;

			std::string ToString() const;

		private:
			T_ScalarType m_Data[T_Width * T_Height];
		};

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>::Matrix(bool doInit)
			: m_Data()
		{
			if (!doInit)
			{
				return;
			}

			for (size_t i = 0; i < T_Width * T_Height; ++i)
			{
				m_Data[i] = 0.0;
			}
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>::Matrix(const Matrix<T_Height, T_Width, T_ScalarType>& other)
			: m_Data()
		{
			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				m_Data[i] = other.m_Data[i];
			}
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>::~Matrix()
		{
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>& Matrix<T_Height, T_Width, T_ScalarType>::operator=(
			const Matrix<T_Height, T_Width, T_ScalarType>& other)
		{
			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				m_Data[i] = other.m_Data[i];
			}
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType> Matrix<T_Height, T_Width, T_ScalarType>::operator+(
			const Matrix<T_Height, T_Width, T_ScalarType>& other) const
		{
			Matrix<T_Height, T_Width, T_ScalarType> result(false);

			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				result.m_Data[i] = m_Data[i] + other.m_Data[i];
			}

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>& Matrix<T_Height, T_Width, T_ScalarType>::operator+=(
			const Matrix<T_Height, T_Width, T_ScalarType>& other) const
		{
			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				m_Data[i] += other.m_Data[i];
			}

			return *this;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType> Matrix<T_Height, T_Width, T_ScalarType>::operator-(
			const Matrix<T_Height, T_Width, T_ScalarType>& other) const
		{
			Matrix<T_Height, T_Width, T_ScalarType> result(false);

			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				result.m_Data[i] = m_Data[i] - other.m_Data[i];
			}

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>& Matrix<T_Height, T_Width, T_ScalarType>::operator-=(
			const Matrix<T_Height, T_Width, T_ScalarType>& other) const
		{
			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				m_Data[i] -= other.m_Data[i];
			}

			return *this;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Vector<T_Height, T_ScalarType> Matrix<T_Height, T_Width, T_ScalarType>::operator[](const size_t columnNumber)
		{
			if (columnNumber >= T_Width)
			{
				// TODO : Throw exception
				throw - 1;
			}

			return Vector<T_Height, T_ScalarType>(columnNumber, m_Data + columnNumber * T_Height);
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Width, T_Height> Matrix<T_Height, T_Width, T_ScalarType>::T() const
		{
			Matrix<T_Width, T_Height, T_ScalarType> result(false);

			for (size_t y = 0; y < T_Width; ++y)
			{
				int resultRowOffset = y * T_Height;

				for (size_t x = 0; x < T_Height; ++x)
				{
					result.m_Data[resultRowOffset + x] = m_Data[x * T_Width + y];
				}
			}

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline T_ScalarType Matrix<T_Height, T_Width, T_ScalarType>::Deteterminant() const
		{
			if constexpr (T_Width != T_Height)
			{
				return 0;
			}

			else if constexpr (T_Width == 0)
			{
				return 0;
			}

			else if constexpr (T_Width == 1)
			{
				return m_Data[0];
			}

			else if constexpr (T_Width == 2)
			{
				return m_Data[0] * m_Data[3] - m_Data[2] * m_Data[1];
			}

			else if constexpr (T_Width == 3)
			{
				return m_Data[0] * m_Data[4] * m_Data[8] + m_Data[1] * m_Data[5] * m_Data[6]
					+ m_Data[2] * m_Data[3] * m_Data[7] - m_Data[6] * m_Data[4] * m_Data[2]
					- m_Data[7] * m_Data[5] * m_Data[0] - m_Data[8] * m_Data[1] * m_Data[3];
			}

			else
			{
				// Do laplace expansion

				// find row or column with least non zero values

				size_t columnZeroCount[T_Width];
				size_t rowZeroCount[T_Height];

				for (size_t column = 0; column < T_Width; ++column)
				{
					size_t columnBegin = column * T_Width;

					for (size_t row = 0; row < T_Height; ++row)
					{
						if (m_Data[columnBegin + row] < JG_MATH_MATRIX_EPSILON)
						{
							columnZeroCount[column] += 1;
							rowZeroCount[row] += 1;
						}
					}
				}

				size_t mostZeroCountColumns = 0;
				size_t mostZeroCountColumnNumber = 0;

				for (size_t column = 0; column < T_Width; ++column)
				{
					size_t zeroCount = columnZeroCount[column];

					if (zeroCount == T_Width)
					{
						return 0;
					}

					if (zeroCount > mostZeroCountColumns)
					{
						mostZeroCountColumns = zeroCount;
						mostZeroCountColumnNumber = column;
					}
				}

				size_t mostZeroCountRows = 0;
				size_t mostZeroCountRowNumber = 0;

				for (size_t row = 0; row < T_Height; ++row)
				{
					size_t zeroCount = rowZeroCount[row];

					if (zeroCount == T_Height)
					{
						return 0;
					}

					if (zeroCount > mostZeroCountRows)
					{
						mostZeroCountRows = zeroCount;
						mostZeroCountRowNumber = row;
					}
				}

				if (mostZeroCountColumns > mostZeroCountRows)
				{
					// Expand through the given column

					T_ScalarType sum = 0;
					size_t columnStart = mostZeroCountColumnNumber * T_Height;

					for (size_t row = 0; row < T_Height; ++row)
					{
						T_ScalarType summand = m_Data[columnStart + row];
						if (summand < JG_MATH_MATRIX_EPSILON)
						{
							continue;
						}

						summand *= this->SubMatrix(mostZeroCountColumnNumber, row).Deteterminant();

						if ((row + mostZeroCountColumnNumber) % 2 == 0)
						{
							sum -= summand;
						}
						else
						{
							sum += summand;
						}
					}

					return sum;
				}
				else
				{
					// Expand through the given row

					T_ScalarType sum = 0;

					for (size_t column = 0; column < T_Width; ++column)
					{
						T_ScalarType summand = m_Data[column * T_Height + mostZeroCountRowNumber];
						if (summand < JG_MATH_MATRIX_EPSILON)
						{
							continue;
						}

						summand *= this->SubMatrix(column, mostZeroCountRowNumber).Deteterminant();

						if ((mostZeroCountRowNumber + column) % 2 == 0)
						{
							sum -= summand;
						}
						else
						{
							sum += summand;
						}
					}

					return sum;
				}
			}
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline Matrix<T_Height - 1, T_Width - 1, T_ScalarType> Matrix<T_Height, T_Width, T_ScalarType>::SubMatrix(
			size_t column, size_t row) const
		{
			// Return Matrix without given row and column
			if (T_Height == 0 || T_Width == 0)
			{
				throw - 1;
			}

			Matrix<T_Height - 1, T_Width - 1, T_ScalarType> result(false);

			size_t resultColumn = 0;
			size_t resultRow = 0;

			for (size_t sourceColumn = 0; sourceColumn < T_Width; ++sourceColumn)
			{
				if (column == sourceColumn)
				{
					continue;
				}

				size_t columnStart = sourceColumn * T_Height;
				size_t resultColumnStart = resultColumn * (T_Height - 1);

				for (size_t sourceRow = 0; sourceRow < T_Height; ++sourceRow)
				{
					if (row == sourceRow)
					{
						continue;
					}

					size_t sourcePosition = columnStart + sourceRow;
					size_t resultPosition = resultColumnStart + resultRow;

					result.m_Data[resultPosition] = this->m_Data[sourcePosition];

					resultRow++;
				}

				resultColumn++;
				resultRow = 0;
			}

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		inline std::string Matrix<T_Height, T_Width, T_ScalarType>::ToString() const
		{
			std::string result = "( ";

			for (size_t column = 0; column < T_Width; ++column)
			{
				result += "( ";

				size_t columnStart = column * T_Height;

				for (size_t row = 0; row < T_Height; ++row)
				{
					result += std::to_string(m_Data[columnStart + row]);
					result += (row == T_Height - 1) ? "" : ", ";
				}

				result += " )";
				result += (column == T_Width - 1) ? "" : ", ";
			}

			result += " )";

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		template <size_t T_OtherWidth>
		inline Matrix<T_Height, T_OtherWidth, T_ScalarType> Matrix<T_Height, T_Width, T_ScalarType>::operator*(
			const Matrix<T_Width, T_OtherWidth, T_ScalarType>& other) const
		{
			Matrix<T_Height, T_OtherWidth, T_ScalarType> result(false);

			for (size_t x = 0; x < T_OtherWidth; ++x)
			{
				for (size_t y = 0; y < T_Height; ++y)
				{

					size_t otherColumnBegin = x * T_Width;
					size_t resultPosition = x * T_Height + y;

					result.m_Data[resultPosition] = 0.0;

					for (size_t n = 0; n < T_Width; ++n)
					{
						result.m_Data[resultPosition] += m_Data[n * T_Height + y] * other.m_Data[otherColumnBegin + n];
					}
				}
			}

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		template <typename T_OtherScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType> Matrix<T_Height, T_Width, T_ScalarType>::operator*(
			const T_OtherScalarType other) const
		{
			Matrix<T_Height, T_Width, T_ScalarType> result(false);

			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				result.m_Data[i] = m_Data[i] * other;
			}

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		template <typename T_OtherScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>& Matrix<T_Height, T_Width, T_ScalarType>::operator*=(
			const T_OtherScalarType other) const
		{
			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				m_Data[i] *= other;
			}

			return *this;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		template <typename T_OtherScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType> Matrix<T_Height, T_Width, T_ScalarType>::operator/(
			const T_OtherScalarType other) const
		{
			Matrix<T_Height, T_Width, T_ScalarType> result(false);

			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				result.m_Data[i] = m_Data[i] / other;
			}

			return result;
		}

		template <size_t T_Height, size_t T_Width, typename T_ScalarType>
		template <typename T_OtherScalarType>
		inline Matrix<T_Height, T_Width, T_ScalarType>& Matrix<T_Height, T_Width, T_ScalarType>::operator/=(
			const T_OtherScalarType other) const
		{
			for (size_t i = 0; i < T_Height * T_Width; ++i)
			{
				m_Data[i] /= other;
			}

			return *this;
		}

		template <size_t T_Height, typename T_ScalarType>
		inline Vector<T_Height, T_ScalarType>::Vector(size_t columnIndex, T_ScalarType* data)
			: m_BeginData(data)
			, m_ColumnIndex(columnIndex)
		{
		}

		template <size_t T_Height, typename T_ScalarType>
		inline T_ScalarType& Vector<T_Height, T_ScalarType>::operator[](const size_t rowNumber)
		{
			if (rowNumber >= T_Height)
			{
				// TODO : Throw exception
				throw - 1;
			}

			return m_BeginData[rowNumber];
		}

		template <size_t T_Height, typename T_ScalarType>
		inline Vector<T_Height, T_ScalarType>::Vector(const Vector<T_Height, T_ScalarType>& other)
		{
			m_BeginData = other.m_BeginData;
			m_ColumnIndex = other.m_ColumnIndex;
		}

		template <size_t T_Height, typename T_ScalarType>
		inline Vector<T_Height, T_ScalarType> Vector<T_Height, T_ScalarType>::operator=(
			Vector<T_Height, T_ScalarType>& other)
		{
			Vector<T_Height, T_ScalarType> result(other);
			return result;
		}

	} // Math

} // JG